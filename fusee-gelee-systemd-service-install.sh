#!/bin/sh
#
# This script will create, enable, and start a systemd service
# that runs the fusee-gelee payload injector every few seconds.

SERVICE_NAME=fusee-gelee
INSTALL_PATH=/opt/${SERVICE_NAME}
SERVICE_ABSOLUTE_PATH=/etc/systemd/system/${SERVICE_NAME}.service
DAEMON_PATH=${INSTALL_PATH}/daemon.sh

create_service() {
	printf 'Creating daemon daemon.sh at %s' $DAEMON_PATH
	printf '#!/bin/sh
#
# Script to run fusee-gelee launcher
while true; do
	%s/fusee-launcher.py -w %s/fusee.bin
done
' ${INSTALL_PATH} ${INSTALL_PATH} > ${DAEMON_PATH}
	chmod +x ${DAEMON_PATH}

	printf 'Creating systemd service %s at %s\n' $SERVICE_NAME $SERVICE_ABSOLUTE_PATH
	printf '# Contents of %s
[Unit]
Description=Fusee Gelee Switch payload injector
After=network.target

[Service]
Type=simple
Restart=always
ExecStart=%s

[Install]
WantedBy=multi-user.target
' ${SERVICE_ABSOLUTE_PATH} ${DAEMON_PATH} > ${SERVICE_ABSOLUTE_PATH}
	systemctl daemon-reload
	systemctl enable $SERVICE_NAME
	systemctl start  $SERVICE_NAME
}

clone_dependencies() {
	printf 'Creating directory %s\n' $INSTALL_PATH
	mkdir -p ${INSTALL_PATH}
	printf 'Cloning fusee-launcher to %s\n' $INSTALL_PATH
	git clone https://github.com/Qyriad/fusee-launcher.git ${INSTALL_PATH}

	HEKATE_ZIP_PATH=${INSTALL_PATH}/hekate.zip
	printf 'Downloading hekate to %s\n' $HEKATE_ZIP_PATH
	curl -L -o $HEKATE_ZIP_PATH \
		https://github.com/CTCaer/hekate/releases/download/v5.3.4/hekate_ctcaer_5.3.4_Nyx_0.9.5.zip
	unzip $HEKATE_ZIP_PATH -d $INSTALL_PATH
	mv ${INSTALL_PATH}/hekate_ctcaer_5.3.4.bin ${INSTALL_PATH}/fusee.bin
}

clone_dependencies
create_service
